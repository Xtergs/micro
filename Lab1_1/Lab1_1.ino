/*
 Name:		Lab1_1.ino
 Created:	1/31/2018 11:28:30 AM
 Author:	RRR
*/

const int port = 12;
// the setup function runs once when you press reset or power the board
void setup() {
	pinMode(port, OUTPUT);
}

// the loop function runs over and over again until power down or reset
void loop() {
	digitalWrite(port, HIGH);
	delay(2 * 1000);
	digitalWrite(port, LOW);
	delay(1000);
	digitalWrite(port, LOW);
	delay(1000);
	digitalWrite(port, HIGH);
	delay(2 * 1000);
}
