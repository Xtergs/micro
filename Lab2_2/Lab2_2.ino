/*
Name:		Lab2_2.ino
Created:	2/7/2018 9:33:39 AM
Author:	RRR
*/

const int readPort = 2;
const int outPort = 10;
// the setup function runs once when you press reset or power the board
void setup() {
	Serial.begin(9600);
}

// the loop function runs over and over again until power down or reset
void loop() {
	int value = analogRead(readPort);
	Serial.println(value);
	float step = value / 1024.0;
	int pwm = step * 256;
	analogWrite(outPort, pwm);
	delay(100);
}