#include <EEPROM.h>

const int length = 16;
char mass[16] = {};
volatile int index = 0;


// the setup function runs once when you press reset or power the board
short pins[] = { 11,10,9 };
short Intervals[] = { 500, 1000, 1500 };
volatile bool values[] = { false, false, false };
short portsLength = 3;
volatile bool flag = false;
const int button = 2;
const int savebutton = 3;

volatile int prevVal = 0;
volatile unsigned long prevTimer = 0;
volatile bool isShow = false;

void Output()
{
	for (int i = 0; i < index; i++)
	{
		Serial.print(mass[i]);
	}
	Serial.println();
}

void isr() {
	int currentVal = digitalRead(button);
	if (currentVal == 1)
	{
		isShow = true;
	}
	else
	{
		isShow = false;
	}
	if (currentVal != prevVal)
	{
		if (currentVal == 0)
		{
			unsigned long currentLong = millis();
			if ((currentLong - prevTimer) > 200)
				mass[index++] = '-';
			else
				mass[index++] = '.';
			prevTimer = currentLong;
			Output();
			if (index == 16)
				index = 0;
		}
		else
		{
			prevTimer = millis();
		}
	}
	prevVal = currentVal;
}
volatile unsigned long saved;
void Save() {
	for (int i = 0; i < index; i++)
	{
      byte v = (byte)(mass[i] - '0');
		EEPROM.write(i, v);
	}
  saved = millis();
}

void ReadCurrentState()
{
	for (int i = 0; i < 16; i++)
	{
		int val = EEPROM.read(i);
		if (val == 255 || val == 0)
			return;
      char xx = (char)(val + '0');
     
		Serial.print(xx);
	}
}

void setup() {
	attachInterrupt(0, isr, CHANGE);
	attachInterrupt(digitalPinToInterrupt(savebutton), Save, RISING);
	pinMode(13, OUTPUT);
	Serial.begin(9600);
	ReadCurrentState();
}

void loop() {
	digitalWrite(13, isShow);
  
  unsigned long current = millis();
  if ((current - saved) > 500)
  {
    ReadCurrentState();
    saved = current;
  }
}
