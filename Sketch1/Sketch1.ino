/*
 Name:		Sketch1.ino
 Created:	1/31/2018 9:01:55 AM
 Author:	RRR
*/
const int even = 12;
const int odd = 11;
// the setup function runs once when you press reset or power the board
void setup() {
	pinMode(even, OUTPUT);
	pinMode(odd, OUTPUT);
}

// the loop function runs over and over again until power down or reset
void loop() {
	digitalWrite(even, HIGH);
	digitalWrite(odd, LOW);
	delay(2 * 1000);
	digitalWrite(even, LOW);
	digitalWrite(odd, HIGH);
	delay(2 * 1000);
}
