/*
 Name:		Lab3_2.ino
 Created:	2/13/2018 11:45:13 AM
 Author:	RRR
*/

#include <MsTimer2.h>

const int led1 = 10;
const int interval = 2;
volatile bool isActive = false;

void work()
{
	isActive = !isActive;
}

// the setup function runs once when you press reset or power the board
void setup() {
	pinMode(led1, OUTPUT);
	MsTimer2::set(interval * 1000, work);
	MsTimer2::start();
}



// the loop function runs over and over again until power down or reset
void loop() {
	digitalWrite(led1, isActive);
}
