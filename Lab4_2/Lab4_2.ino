/*
 Name:		Lab4_3.ino
 Created:	2/19/2018 11:00:59 AM
 Author:	RRR
*/

String mass[20] = {};
int index = 0;
bool readingMode = false;

// the setup function runs once when you press reset or power the board
void setup() {
	Serial.begin(9600);
}

void Send()
{
	Serial.println("----Sending-----");
	for (int i = 0; i < index; i++)
	{
		const char * c = mass[i].c_str();
		Serial.write(c);
	}
  Serial.println();
	Serial.println("----Sent-----");
}

// the loop function runs over and over again until power down or reset
void loop() {
  //delay(100);
  int avaliable = Serial.available();
	if ( avaliable > 0)
	{
		String str = Serial.readString();
		if (readingMode == false && str == "begin")
		{
			index = 0;
			readingMode = true;
			return;
		}
		if (readingMode == true && str == "end")
		{
			readingMode = false;
			return;
		}
		if (readingMode == false && str == "send")
		{

			Send();
			return;
		}
		if (readingMode)
		{
			mass[index++] = str;
			return;
		}
      else
      {
        Serial.println(str);
      }
		
  }
}
